/**This script creates all the tables in the client SQLite database.
NO SEMICOLONS IN THIS FILE EXCEPT AT THE END OF EACH QUERY!*/

/**Events that occur at a single point in time.*/
CREATE TABLE Events
(
	event_id INTEGER PRIMARY KEY,
	event_start INTEGER NOT NULL REFERENCES Times(time_id),
	event_end INTEGER REFERENCES Times(time_id),
	event_name TEXT,
	event_description TEXT,
	event_location INTEGER REFERENCES Locations(location_id),
	event_work_minutes INTEGER,
	event_work_percent REAL,
	event_priority INTEGER
);

/**Times, which can be either anonymous or named.*/
CREATE TABLE Times
(
	time_id INTEGER PRIMARY KEY,
	time_name TEXT,
	year INTEGER,
	month INTEGER,
	day INTEGER,
	hour INTEGER,
	minute INTEGER
);

/**Locations are currently just strings.*/
CREATE TABLE Locations
(
	location_id INTEGER PRIMARY KEY,
	location_name TEXT NOT NULL,
	location_description TEXT
);

/**Labels, which apply to Events.*/
CREATE TABLE Labels
(
	label_id INTEGER PRIMARY KEY,
	label_name TEXT,
	label_description TEXT
);

/**Pairings of labels and events.*/
CREATE TABLE AppliedLabels
(
	label_id INTEGER NOT NULL REFERENCES Labels(label_id),
	event_id INTEGER NOT NULL REFERENCES Events(event_id),
	CONSTRAINT cns_unique UNIQUE(label_id, event_id)
);


/**Events that repeat at an even interval.*/
CREATE TABLE EvenRepetitions
(
	repetition_id INTEGER PRIMARY KEY,
	repetition_time INTEGER NOT NULL REFERENCES Times(time_id),
	source_event INTEGER NOT NULL REFERENCES Events(event_id)
);
