package imp.cal;

import java.io.*;
import java.util.Scanner;

import android.content.Context;
import android.database.sqlite.*;


/**The persistence layer of this application.
 * Handles connecting to the database, loading SQL scrips, and related stuff.
 * 
 * This class handles a special locator file that stores the location of the database;
 * this location can be unspecified, meaning the app doesn't have a database (yet).
 * If this file does not exist, then there is no database file (yet).
 * 
 * Unless otherwise mentioned ("cheap"), methods in this class perform file-related operations
 * and shouldn't be called on the UI thread.*/
public class Persistence
{
	/**Name of the file that contains the absolute path to the database file.
	 * If this directory file does not exist, then the database does not exist.*/
	private static final String DATABASE_LOCATOR_FILENAME = "database_location";
	/**Path to the SQL scripts directory, relative to the assets directory.*/
	private static final String SQL_SCRIPTS_PATH = "sql/";
	
	
	private final Context context;
	/**Lazily loaded via databaseFile(); other code shouldn't use this directly.*/
	private File databaseFile = null;
	/**Whether or not the class has actually tried to find the database using the locator file.*/
	private boolean searchedForDatabase = false;
	
	
	
	/**Cheap.*/
	public Persistence(Context context)
	{
		this.context = context;
	}
	
	

	/**Returns the file that specifies the database location.*/
	private File databaseLocatorFile(){return new File(context.getFilesDir().getAbsolutePath(), DATABASE_LOCATOR_FILENAME);}
	
	
	/**Returns whether or not the database location has been specified.
	 * Finds out if necessary.
	 * @throws FileNotFoundException should never happen*/
	public boolean databaseLocationSpecified() throws FileNotFoundException
	{
		if(!searchedForDatabase)
		{
			File f = databaseLocatorFile();
			if(f.exists())
				databaseFile = new File(Utils.readFile(databaseLocatorFile()));
			searchedForDatabase = true;
		}
		return databaseFile != null;
	}
	
	/**Gets the location of the database, or null if unspecified.
	 * @throws FileNotFoundException this should never happen*/
	public File getDatabaseLocation() throws FileNotFoundException
	{
		if(!databaseLocationSpecified())
			return null;
		return databaseFile;
	}
	
	
	
	/**Gets a SQL script from the sql script assets.
	 * Name should not include the .sql file extension.*/
	public String getSQLScript(String name) throws IOException
	{
		String path = SQL_SCRIPTS_PATH + name + ".sql";
		InputStream in = context.getAssets().open(path);
		
		//stackoverflow.com/a/5445161    \A apparently means read to end of file
		@SuppressWarnings("resource")//it complains I never close the first scanner; who cares?
		Scanner scan = new Scanner(in).useDelimiter("\\A");
	    String out = scan.hasNext() ? scan.next() : "";
	    
	    scan.close();
	    return out;
	}
	
	
	/**Connects to the database, creating (but not initializing) it if necessary.
	 * @throws FileNotFoundException this should never happen
	 * @throws IllegalStateException if the database location isn't specified*/
	public SQLiteDatabase connectToDatabase() throws FileNotFoundException, IllegalStateException
	{
		if(!databaseLocationSpecified())
			throw new IllegalStateException("The database location is unspecified");
		return SQLiteDatabase.openDatabase(getDatabaseLocation().getPath(), null,
				SQLiteDatabase.OPEN_READWRITE | SQLiteDatabase.CREATE_IF_NECESSARY);
	}
	
	
	
	
	
	/**Sets the location of the database file, writing the location to the database location file.
	 * Pass in null to make the database location unspecified.
	 * @throws IOException */
	public void setDatabaseLocation(File location) throws IOException
	{
		if(location == null)
		{
			databaseLocatorFile().delete();
		}
		else
		{
			String str = location.getAbsolutePath();
			FileWriter wr = new FileWriter(databaseLocatorFile());
			wr.write(str);
			wr.close();
		}
	}
	
	public File getDefaultDatabaseLocation(){return new File(context.getFilesDir(), "data.sqlite");}
}
