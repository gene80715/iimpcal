package imp.cal.background;

import imp.cal.Persistence;
import imp.cal.Schema;
import imp.cal.bridge.BackgroundTask;

/**Creates the database at the default location.
 * Database should be unspecified when this is called.
 * Returns any errors that occurred, or null if the operation was successful.*/
public class CreateDatabaseTask implements BackgroundTask<Void, Exception>
{
	private final Persistence persist;
	public CreateDatabaseTask(Persistence persist){this.persist = persist;}
	
	@Override public Exception doInBackground(Void... args)
	{
		try
		{
			Schema.initDatabase(persist);
			return null;
		}
		catch(Exception ex)
		{
			return ex;
		}
	}
}
