package imp.cal.bridge;

public interface UITask<Result>
{
	public void onPostExecute(Result result);
}
