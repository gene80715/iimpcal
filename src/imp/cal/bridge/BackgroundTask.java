package imp.cal.bridge;

public interface BackgroundTask<Param,Result>
{
	public Result doInBackground(Param... list);
}
