package imp.cal.bridge;

import android.os.AsyncTask;

/**A configurable AsyncTask that takes different objects to run its various functions.
 * Currently does not do any progress-related stuff; this will come sometime.*/
public class ConfigTask<Params,Progress,Result> extends AsyncTask<Params,Progress,Result>
{
	private final BackgroundTask<Params,Result> backgroundTask;
	private final UITask<Result> postTask;
	
	
	/**@param postTask the task to be run after the background task finishes,
	 * being passed the result returned by the background task.*/
	public ConfigTask(BackgroundTask<Params,Result> backgroundTask, UITask<Result> postTask)
	{
		this.backgroundTask = backgroundTask;
		this.postTask = postTask;
	}
	
	
	/**Stores one or more new Whens in the database.
	 * Returns any error that prevented successful storage, or null if everything went well.*/
	@Override protected Result doInBackground(Params... list)
	{
		return backgroundTask.doInBackground(list);
	}
	
	
	@Override protected void onProgressUpdate(Progress... progress)
	{
		throw new UnsupportedOperationException("TODO");//TODO
	}
	
	
	@Override protected void onPostExecute(Result result)
	{
		postTask.onPostExecute(result);
	}

}
