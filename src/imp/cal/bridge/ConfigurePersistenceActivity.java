package imp.cal.bridge;

import java.io.File;
import java.io.FileNotFoundException;

import imp.cal.Persistence;
import imp.cal.R;
import imp.cal.Schema;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.*;

/**Activity that lets the user set the database location.
 * When the activity is created, it begins by looking up the database status (whether it is specified and exists).
 * Depending on the result, the user may be allowed to set the database location, or edit/delete it.*/
public class ConfigurePersistenceActivity extends Activity
{
	private static enum State{looking, unspecified, specified};
	
	private Persistence persist;
	private State state = State.looking;
	
	
	@Override protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_configure_persistence);
		persist = new Persistence(getApplicationContext());
		
		final LookupStatusTask backgroundTask = new LookupStatusTask();
		final PostLookupStatusTask postTask = new PostLookupStatusTask();
		ConfigTask<Void, Void, DBStatus> task = new ConfigTask<Void, Void, DBStatus>(backgroundTask, postTask);
		task.execute();
	}
	
	@Override public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.init_persistence, menu);
		return true;
	}
	
	
	/**Sets the UI to reflect that the database location is unspecified.*/
	private void setUIDatabaseUnspecified()
	{
		if(state == State.unspecified)
			return;
		
		ProgressBar bar = (ProgressBar)findViewById(R.id.progress);
		bar.setIndeterminate(false);
		bar.setProgress(0);
		
		//update text displays
		TextView shortStatus = (TextView)findViewById(R.id.label_status_short);
		shortStatus.setText(R.string.ui_cfgPersist_status_unspecified_short);
		shortStatus.setTextAppearance(getApplicationContext(), R.style.Text_bad);
		TextView longStatus = (TextView)findViewById(R.id.label_status_long);
		longStatus.setText(R.string.ui_cfgPersist_status_unspecified_long);
		longStatus.setTextAppearance(getApplicationContext(), R.style.Text_bad);
		
		//update buttons
		Button btn1 = (Button)findViewById(R.id.button1);
		Button btn2 = (Button)findViewById(R.id.button2);
		btn1.setText("Create in Default Location");
		btn1.setEnabled(true);
		btn1.setVisibility(View.VISIBLE);
		btn2.setText("Pick Custom Location");
		btn2.setEnabled(false);
		btn2.setVisibility(View.VISIBLE);
		
		state = State.unspecified;
	}
	
	/**Sets the UI to reflect that the database location is specified.*/
	private void setUIDatabaseSpecified()
	{
		if(state == State.specified)
			return;
		
		ProgressBar bar = (ProgressBar)findViewById(R.id.progress);
		bar.setIndeterminate(false);
		bar.setProgress(bar.getMax());
		
		//update text displays
		TextView shortStatus = (TextView)findViewById(R.id.label_status_short);
		shortStatus.setText(R.string.ui_cfgPersist_status_specified_short);
		shortStatus.setTextAppearance(getApplicationContext(), R.style.Text_ok);
		TextView longStatus = (TextView)findViewById(R.id.label_status_long);
		longStatus.setText(R.string.ui_cfgPersist_status_specified_long);
		longStatus.setTextAppearance(getApplicationContext(), R.style.Text_ok);
		
		//update buttons
		Button btn1 = (Button)findViewById(R.id.button1);
		Button btn2 = (Button)findViewById(R.id.button2);
		btn1.setText("Delete");
		btn1.setEnabled(true);
		btn1.setVisibility(View.VISIBLE);
		btn2.setText("Change Location");
		btn2.setEnabled(false);
		btn2.setVisibility(View.VISIBLE);
		
		state = State.specified;
	}
	
	/**Disables buttons and puts the progress bar in indeterminate mode.*/
	private void busy()
	{
		((Button)findViewById(R.id.button1)).setEnabled(false);
		((Button)findViewById(R.id.button2)).setEnabled(false);
		((ProgressBar)findViewById(R.id.progress)).setIndeterminate(true);
	}
	
	
	
	/**Handler for button 1.*/
	public void onButton1(View v)
	{
		switch(state)
		{
			case unspecified:
				busy();
				
				//create database in default location
				BackgroundTask<Void, Exception> backLookupTask = new CreateDatabaseTask();
				UITask<Exception> postLookupTask = new PostCreateDatabaseTask();
				ConfigTask<Void, Void, Exception> task = new ConfigTask<Void, Void, Exception>(backLookupTask, postLookupTask);
				task.execute();
				break;
				
			case specified:
				busy();
				
				new DeleteDatabaseTask().execute();
				break;
				
			default:
				throw new IllegalStateException();//XXX unexpected error handling
		}
	}
	
	
	
	
	/**Represents the status of the database.*/
	private class DBStatus
	{
		public final boolean databaseSpecified;
		/**The file where the database is located, or null if unspecified.*/
		public final File file;
		/**Whether or not the database file actually exists (false if the database is unspecified).*/
		public final boolean fileExists;
		
		/**The database file is specified, and is located at the provided file.
		 * Only call this constructor on a background thread.*/
		public DBStatus(File file)
		{
			this.databaseSpecified = true;
			this.file = file;
			this.fileExists = file.exists();
		}
		
		/**The database file is unspecified.*/
		public DBStatus()
		{
			this.databaseSpecified = false;
			this.file = null;
			this.fileExists = false;
		}
	}
	
	
	/**Background task that gets the database status.*/
	private class LookupStatusTask implements BackgroundTask<Void, DBStatus>
	{
		@Override public DBStatus doInBackground(Void... list)
		{
			try
			{
				try{Thread.sleep(3000);}catch(InterruptedException ex){}//TODO DEBUG
				if(!persist.databaseLocationSpecified())
					return new DBStatus();
				File file = persist.getDatabaseLocation();
				return new DBStatus(file);
			}
			catch(FileNotFoundException ex)
			{
				throw new UnsupportedOperationException(ex);//XXX unexpected error handling
			}
		}
	}
	
	/**UI task that handles the result of a database status lookup.*/
	private class PostLookupStatusTask implements UITask<DBStatus>
	{
		@Override public void onPostExecute(DBStatus result)
		{
			if(!result.databaseSpecified)
				setUIDatabaseUnspecified();
			else
				setUIDatabaseSpecified();
		}
	}
	
	/**Specifies the database location as the default location, then initializes the database.
	 * Returns any error that occured, or null if the operation was successful.*/
	private class CreateDatabaseTask implements BackgroundTask<Void, Exception>
	{

		@Override public Exception doInBackground(Void... paths)
		{
			try{Thread.sleep(3000);}catch(InterruptedException ex){}//TODO DEBUG
			try
			{
				persist.setDatabaseLocation(persist.getDefaultDatabaseLocation());
				Schema.initDatabase(persist);
				return null;
			}
			catch(Exception ex)
			{
				return ex;
			}
		}
	}
	
	/**UI task that runs after the database is specified by the user.*/
	private class PostCreateDatabaseTask implements UITask<Exception>
	{

		@Override public void onPostExecute(Exception problem)
		{
			if(problem == null)
				setUIDatabaseSpecified();
			else
				throw new UnsupportedOperationException();//XXX unexpected error handling
		}
	}
	
	
	private class DeleteDatabaseTask extends AsyncTask<Void, Void, Exception>
	{
		@Override protected Exception doInBackground(Void... params)
		{
			try{Thread.sleep(3000);}catch(InterruptedException ex){}//TODO DEBUG
			try
			{
				File file = persist.getDatabaseLocation();
				file.delete();
				return null;
			}
			catch(Exception ex)
			{
				return ex;
			}
		}
		
		@Override protected void onPostExecute(Exception problem)
		{
			if(problem == null)
				setUIDatabaseUnspecified();
			else
				throw new UnsupportedOperationException();//XXX unexpected error handling
		}
	}
}
