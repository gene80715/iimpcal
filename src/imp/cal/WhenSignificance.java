package imp.cal;

import static java.util.Calendar.*;

/**Tracks whether or not each of the fields in a When (a java.util.Calendar) are significant.
 * For definition of fields, see imp.cal.When.
 * 
 * Currently, lets you modify its fields directly; there's no point in creating a load of getters/setters for this.*/
public class WhenSignificance
{
	public boolean year, month, day, hour, minute;
	
	public WhenSignificance(boolean year, boolean month, boolean day, boolean hour, boolean minute)
	{
		this.year = year;
		this.month = month;
		this.day = day;
		this.hour = hour;
		this.minute = minute;
	}
	
	/**Creates a WhenSignificance where all fields are false (insignificant).*/
	public WhenSignificance()
	{
		this(false, false, false, false, false);
	}
	
	
	/**Set a field.*/
	public void set(int field, boolean value)
	{
		switch(field)
		{
			case YEAR:
				this.year = value;
				break;
			case MONTH:
				this.month = value;
				break;
			case DAY_OF_MONTH:
				this.day = value;
				break;
			case HOUR_OF_DAY:
				this.hour = value;
				break;
			case MINUTE:
				this.minute = value;
				break;
			default:
				throw new IllegalArgumentException("Invalid field: " + field);
		}
	}
	
	/**Gets a field.*/
	public boolean get(int field)
	{
		switch(field)
		{
			case YEAR:
				return year;
			case MONTH:
				return month;
			case DAY_OF_MONTH:
				return day;
			case HOUR_OF_DAY:
				return hour;
			case MINUTE:
				return minute;
			default:
				throw new IllegalArgumentException("Invalid field: " + field);
		}
	}
	
	
	
	/**Returns whether or not all the fields of both WhenSignificance's are identical.*/
	@Override public boolean equals(Object other)
	{
		if(!(other instanceof WhenSignificance))
			return false;
		WhenSignificance that = (WhenSignificance)other;
		return this.year == that.year && this.month == that.month && this.day == that.day && this.hour == that.hour && this.minute == that.minute;
	}
	
	
	@Override
	public WhenSignificance clone()
	{
		return new WhenSignificance(year, month, day, hour, minute);
	}
}
