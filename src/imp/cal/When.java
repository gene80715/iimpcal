package imp.cal;

import java.util.Calendar;

import static java.util.Calendar.*;

/**The date/time when something happens.
 * This consists of both a set of numeric values (what year, etc. as a java.util.Calendar)
 * and a set of booleans that specify whether each field is "significant" (a WhenSignificance),
 * plus an integer id (globally unique).
 * 
 * This and related classes use the following fields: year, month, day [of month], hour [of day], minute;
 * these are specified by the constants in java.util.Calendar.
 * An id of -1 means this When doesn't have an id yet.
 * This class uses 24-hour time.
 * 
 * Currently, the fields are public to allow modification; this might change sometime.
 * Some fields are final to allow modification (e.g. changing the day)
 * but not reassignment (e.g. changing the calendar instance).*/
public class When
{
	/**A list of the java.util.Calendar field identifiers, in descending order of significance.*/
	private static final int[] fieldList = {YEAR, MONTH, DAY_OF_MONTH, HOUR_OF_DAY, MINUTE};
	
	public long id = -1;
	public String name = null;
	public final Calendar calendar;
	public final WhenSignificance significance;
	
	
	public When(long id, Calendar calendar, WhenSignificance significance)
	{
		this.id = id;
		this.calendar = calendar;
		this.significance = significance;
	}
	
	/**Creates a When with no id (-1)*/
	public When(Calendar calendar, WhenSignificance significance)
	{
		this.calendar = calendar;
		this.significance = significance;
	}
	
	/**Creates a completely blank When with no id (-1) and all fields being insignificant.*/
	public When()
	{
		calendar = Calendar.getInstance();
		significance = new WhenSignificance();
	}
	
	
	/**Returns whether or not this When is reasonable.
	 * A When is reasonable if there is no significant field that is smaller (month < year) than a non-significant field.*/
	public boolean isReasonable()
	{
		boolean forbidSignificance = false;
		for(int field : fieldList)
		{
			System.out.println("field: " + field);//DEBUG
			boolean current = significance.get(field);
			if(current)
			{
				if(forbidSignificance)
					return false;
			}
			else
				forbidSignificance = true;
		}
		
		return true;
	}
	
	
	/**Sets a field, and marks it as significant.*/
	public void set(int field, int value)
	{
		significance.set(field,  true);
		calendar.set(field, value);
	}
	
	
	/**Sets a field and marks it as significant, unless it is already significant,
	 * in which case this methods throws an IllegalStateException instead.*/
	public void setNoOverwrite(int field, int value) throws IllegalStateException
	{
		if(significance.get(field))
			throw new IllegalStateException();
		significance.set(field, true);
		calendar.set(field, value);
	}
	
	/**Marks a field as not significant.*/
	public void unset(int field)
	{
		significance.set(field, false);
	}
	
	
	
	/**Two When's are unequal if they do not have exactly the same set of significant fields.
	 * They also differ if any of their significant fields are not identical.*/
	@Override public boolean equals(Object other)
	{
		if(!(other instanceof When))
			return false;
		When that = (When)other;
		return significantFieldsEqual(that);
	}
	
	
	/**Like equals, but uses a shortcut:
	 * two When's are always equal if they have the same ID (except -1).*/
	public boolean lazyEquals(Object other)
	{
		if(!(other instanceof When))
			return false;
		When that = (When)other;
		if(this.id != -1 && this.id == that.id)
			return true;
		return significantFieldsEqual(that);
	}
	
	
	/**Determines whether two When's have the same set of significant fields
	 * and the same values in each field.*/
	private boolean significantFieldsEqual(When that)
	{
		if(!that.significance.equals(this.significance))
			return false;

		if(significance.year && this.calendar.get(YEAR) != that.calendar.get(YEAR))
			return false;
		if(significance.month && this.calendar.get(MONTH) != that.calendar.get(MONTH))
			return false;
		if(significance.day && this.calendar.get(DAY_OF_MONTH) != that.calendar.get(DAY_OF_MONTH))
			return false;
		if(significance.hour && this.calendar.get(HOUR) != that.calendar.get(HOUR))
			return false;
		if(significance.minute && this.calendar.get(MINUTE) != that.calendar.get(MINUTE))
			return false;
		
		return true;
	}
	
	
	@Override public When clone()
	{
		Calendar calendar = (Calendar)this.calendar.clone();
		WhenSignificance significance = (WhenSignificance)this.significance.clone();
		return new When(this.id, calendar, significance);
	}
}
