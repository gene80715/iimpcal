package imp.cal.unit;

import static org.junit.Assert.*;
import static java.util.Calendar.*;

import java.util.Calendar;

import imp.cal.When;
import imp.cal.WhenParse;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;



@RunWith(JUnit4.class)
public class TimeParseTest
{
	
    @Test
    public void thisAlwaysPasses()
    {

    }

    @Test
    @Ignore
    public void thisIsIgnored()
    {
    	
    }
    
    @Test
    @Ignore
    public void thisAlwaysFails()
    {
    	fail("always fails");
    }
    
    /**Tests creating a When representing today.*/
    @Test
    @Ignore
    public void testToday()
    {
    	When parsed = WhenParse.parse("today");
    	When answer = new When();
    	answer.set(YEAR, Calendar.getInstance().get(YEAR));
    	answer.set(MONTH, Calendar.getInstance().get(MONTH));
    	answer.set(DAY_OF_MONTH, Calendar.getInstance().get(DAY_OF_MONTH));
    	
    	assert(answer.equals(parsed));
    }
    
    /**Tests creating a When representing tomorrow.*/
    @Test
    public void testTomorrow()
    {
    	When parsed = WhenParse.parse("tomorrow");
    	When answer = new When();
    	answer.set(YEAR, Calendar.getInstance().get(YEAR));
    	answer.calendar.add(DAY_OF_MONTH, 1);
    	answer.set(MONTH, Calendar.getInstance().get(MONTH));
    	answer.set(DAY_OF_MONTH, Calendar.getInstance().get(DAY_OF_MONTH));
    	
    	assert(answer.equals(parsed));
    }
    
    /**Tests creating a When representing tomorrow.*/
    @Test
    public void testjan()
    {
    	When parsed = WhenParse.parse("jan");
    	When answer = new When();
    	answer.set(MONTH,JANUARY);
    	
    	assert(answer.equals(parsed));
    }
    
    /**Tests creating a When representing tomorrow.*/
    @Test
    public void testday()
    {
    	When parsed = WhenParse.parse("2014/1/1");
    	When answer = new When();
    	answer.set(MONTH,JANUARY);
    	answer.set(DAY_OF_MONTH, 1);
    	answer.set(YEAR, 2014);
    	
    	assert(answer.equals(parsed));
    }
    
    @Test
    public void testAMPM()
    {
    	When parsed = WhenParse.parse("2014/1/1");
    	When answer = new When();
    	answer.set(MONTH,JANUARY);
    	answer.set(DAY_OF_MONTH, 1);
    	answer.set(YEAR, 2014);
    	
    	assert(answer.equals(parsed));
    }
}