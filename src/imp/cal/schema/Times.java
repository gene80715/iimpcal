package imp.cal.schema;

import imp.cal.Persistence;
import imp.cal.When;

import java.io.IOException;
import java.util.*;

import static java.util.Calendar.*;


/**Includes information about the Times table (CURRENTLY NOT!), as well as methods for interacting with it.*/
public class Times
{
	/*public static final String TABLE = "Times";
	private static final String id = "time_id", name = "time_name",
			year = "year", month = "month", day = "day", hour = "hour", minute = "minute";*/
	
	
	
	/**Stores a completely new When in the database, giving it an ID if necessary.*/
	public static void createWhen(When when, Persistence persist) throws IOException
	{
		String id = (when.id == -1) ? null : when.id + "";
		String name = when.name;
		
		String year = when.significance.get(YEAR) ? when.calendar.get(YEAR) + "" : null;
		String month = when.significance.get(YEAR) ? when.calendar.get(YEAR) + "" : null;
		String day = when.significance.get(YEAR) ? when.calendar.get(YEAR) + "" : null;
		String hour = when.significance.get(YEAR) ? when.calendar.get(YEAR) + "" : null;
		String minute = when.significance.get(YEAR) ? when.calendar.get(YEAR) + "" : null;
		
		String query = persist.getSQLScript("create_time");
		persist.connectToDatabase().rawQuery(query, new String[]{id, name, year, month, day, hour, minute});
	}
}
