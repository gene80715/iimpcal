package imp.cal;


/**Helps with queries.*/
@Deprecated public class Queries
{
	/**Turns an array of column names into a string of the form
	 * (col1, col2, ...)*/
	public static String columns(String[] columnNames)
	{
		String out = "(" + columnNames[0];
		for(int c = 1; c < columnNames.length; c++)
			out += ", " + columnNames[c];
		return out + ")";
	}

}
