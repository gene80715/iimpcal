package imp.cal;

import imp.cal.bridge.*;
import imp.cal.ui.CreateTimeTask;
import imp.cal.ui.PostCreateTimeTask;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class CreateTimeActivity extends Activity
{
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_time);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.create_time, menu); TODO had to comment this out to make the program run
		return true;
	}
	
	/**
	 * This is our button handler that just launches a background task to parse
	 * the input time and inserts it into the database if there were no errors.
	 */
	public void create_time(View view)
	{
		EditText t = new EditText(this);
		t = (EditText) findViewById(R.id.time_entry);
		String timeString = t.getText().toString();
		
		CreateTimeTask backgroundTask = new CreateTimeTask(new Persistence(getApplicationContext()));
		PostCreateTimeTask postTask = new PostCreateTimeTask(this);
		new ConfigTask<String, Void, Exception>(backgroundTask, postTask).execute(timeString);
	}
	
	/**
	 * This method executes when the parse and input AsyncTask completes.
	 * Displays error message or "Sucess!"
	 */
	public void onPostCreateTime(Exception e)
	{
		TextView t = new TextView(this);
		t = (TextView) findViewById(R.id.label_error_message);
		if (e == null)
		{
			t.setText("Success!");
			t.setTextColor(Color.GREEN);
		} else
		{
			t.setText(e.getMessage());
			t.setTextColor(Color.RED);
		}
	}
	
}
