package imp.cal;

import java.io.*;
import java.util.Scanner;

public class Utils
{
	/**Reads an entire input stream.*/
	public static String readStream(InputStream in)
	{
		//stackoverflow.com/a/5445161    \A apparently means read to end of file
		@SuppressWarnings("resource")//it complains I never close the first scanner; who cares?
		Scanner scan = new Scanner(in).useDelimiter("\\A");
	    String out = scan.hasNext() ? scan.next() : "";
	    
	    scan.close();
	    return out;
	}
	
	/**Read an entire file.*/
	public static String readFile(File file) throws FileNotFoundException
	{
		return readStream(new FileInputStream(file));
	}
}
