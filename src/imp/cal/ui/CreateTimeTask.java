package imp.cal.ui;

import imp.cal.Persistence;
import imp.cal.WhenParse;
import imp.cal.When;
import imp.cal.bridge.*;
import imp.cal.schema.Times;

/**Parses a String into a When, then stores it in the database.
 * If the list contains multiple whens, additional ones are ignored.
 * Returns null if no problems occur; otherwise, returns the Exception encountered.*/
public class CreateTimeTask implements BackgroundTask<String,Exception>
{
	private final Persistence persist;
	
	
	public CreateTimeTask(Persistence persist)
	{
		this.persist = persist;
	}
	
	
	@Override public Exception doInBackground(String... list)
	{
		try
		{
			String in = list[0];
			When when = WhenParse.parse(in);
			Times.createWhen(when, persist);
			return null;
		}
		catch(Exception ex)
		{
			return ex;
		}
		
		//TODO log if the list has multiple When's (not supposed to happen)
	}
	
}
