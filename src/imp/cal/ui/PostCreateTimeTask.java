package imp.cal.ui;

import imp.cal.CreateTimeActivity;
import imp.cal.bridge.UITask;

public class PostCreateTimeTask implements UITask<Exception>
{

	private final CreateTimeActivity activity;
	
	public PostCreateTimeTask (CreateTimeActivity activity)
	{
		this.activity = activity;
	}
	
	@Override
	public void onPostExecute(Exception result)
	{
		// TODO Auto-generated method stub
		activity.onPostCreateTime(result);
		
	}

}
