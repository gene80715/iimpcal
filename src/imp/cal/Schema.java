package imp.cal;

import java.io.IOException;

import android.database.sqlite.SQLiteDatabase;

/**Has "stuff" that deals with the whole database.*/
public class Schema
{
	/**Creates all tables, etc.
	 * Should only be run on an empty (or nonexistent-but-specified) database.
	 * @throws IOException */
	public static void initDatabase(Persistence persist) throws IOException
	{
		String wholeQuery = persist.getSQLScript("init");
		String[] queries = wholeQuery.split(";");
		SQLiteDatabase db = persist.connectToDatabase();
		
		//run all queries (there is no query after the last semicolon)
		for(int c = 0; c < queries.length - 1; c++)
		{
			String query = queries[c];
			db.rawQuery(query, null);
		}
	}
}
