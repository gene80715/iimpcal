package imp.cal;

import static java.util.Calendar.*;

import java.util.Calendar;

/**Parses a user-entered string into a When.
 * The syntax will eventually be specified elsewhere, but here's some documentation for now:
 * 
 * This class is case insensitive.
 * It first splits the string into words. Commas at the end of words are ignored.
 * 
 * Any words that match the following are immediately parsed:
 * tomorrow
 * today, yesterday TODO not implemented yet
 * {month names}
 * a[m]/p[m]
 * {weekday names} -- turned into year+month+day
 * 
 * Any words in any of these formats are immediately parsed:
 * y-m-d
 * y/m/d
 * yyyy   -- 4-digit year
 * n      -- 1-2 digit day
 * nth    -- day
 * h:m
 * h:     -- hour without minute
 * 
 * Any unrecognized words are errors.
 * If the user specifies the same field twice, it is an error.
 * Errors are IllegalArgumentExceptions with user-readable messages.
 * 
 * If AM/PM is missing, 24-hour time is used.
 * 
 * Special exception: a word can be "at", in which case the next word must be in the the format h:m or h.*/
public class WhenParse
{
	/**If a string starts with the nth element, it represents month n+1.*/
	private static final String[] MONTH_PREFIXES = {"jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"};
	/**If a string starts with the nth element, it represents the weekday WEEKDAYS[n], as understood by java.util.Calendar.*/
	private static final String[] WEEKDAY_PREFIXES = {"sun", "mon", "tue", "wed", "thu", "fri", "sat"};
	private static final int[] WEEKDAYS = {SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY};
	
	
	
	
	/**See class documentation.*/
	public static When parse(String in) throws IllegalArgumentException
	{
		String[] originalWords = in.split(" ");//used to give better errors
		String[] words = in.toLowerCase().split(" ");
		
		When out = new When();
		
		int amStatus = 0; //0 (unset), 1 (am), 2 (pm)
		int hour = 0;//we can't send this to the Calendar till we know AM/PM; initial value should never be used
		boolean at = false;//true when the previous word was an "at"
		
		
outer:	for(int c = 0; c < words.length; c++)
		{
			String word = words[c];
			
			//chop off trailing comma
			if(word.endsWith(","))
				word = word.substring(0, word.length() - 1);
			
			
			
			/*SPECIAL EXCEPTION for "at"*/
			if(at)
			{
				if(word.contains(":"))
				{
					String[] halves = word.split(":");
					try
					{
						hour = Integer.parseInt(halves[0]);
						int minute = Integer.parseInt(halves[1]);
						if(out.significance.hour)
							throw new IllegalArgumentException("Error in '" + originalWords[c] + "': hour already specified.");
						if(out.significance.minute)
							throw new IllegalArgumentException("Error in '" + originalWords[c] + "': minute already specified.");
						out.calendar.set(MINUTE, minute);
						out.significance.hour = out.significance.minute = true;
						continue outer;
					}
					catch(NumberFormatException ex){}//handled below
					catch(ArrayIndexOutOfBoundsException ex){}//also handled below (triggered if there aren't 2 halves)
				}
				else
					try
					{
						hour = Integer.parseInt(word);
						if(out.significance.hour)
							throw new IllegalArgumentException("Error in '" + originalWords[c] + "': hour already specified.");
						out.significance.hour = true;
						continue outer;
					}
					catch(NumberFormatException ex){}//also handled below
				
				//if we reached this point, "at" wasn't followed by the required format
				throw new IllegalArgumentException("Error in '" + originalWords[c] + "': 'at' wasn't followed by a time.");
			}
			else if(word.equals("at"))
			{
				at = true;
				continue outer;
			}
			
			
			
			
			/*MATCH EXPLICIT STRINGS*/
			
			//check for "tomorrow"
			if(word.equals("tomorrow"))
			{
				if(out.significance.year)
					throw new IllegalArgumentException("Error in '" + originalWords[c] + "': year already specified.");
				if(out.significance.month)
					throw new IllegalArgumentException("Error in '" + originalWords[c] + "': month already specified.");
				if(out.significance.day)
					throw new IllegalArgumentException("Error in '" + originalWords[c] + "': day already specified.");
				out.calendar.set(DAY_OF_MONTH, Calendar.getInstance().get(DAY_OF_MONTH) + 1);
				out.significance.year = out.significance.month = out.significance.day = true;
				continue outer;
				
			}
			
			//check for month names
			for(int monthIndex = 0; monthIndex < MONTH_PREFIXES.length; monthIndex++)
				if(word.startsWith(MONTH_PREFIXES[monthIndex]))
				{
					if(out.significance.month)
						throw new IllegalArgumentException("Error in '" + originalWords[c] + "': month already specified.");
					out.calendar.set(MONTH, monthIndex + 1);
					out.significance.month = true;
					continue outer;
				}
			
			//check for AM/PM
			if(word.equals("am") || word.equals("a"))
			{
				if(amStatus != 0)
					throw new IllegalArgumentException("Error in '" + originalWords[c] + "': AM/PM already specified.");
				amStatus = 1;
				continue outer;
			}
			if(word.equals("pm") || word.equals("p"))
			{
				if(amStatus != 0)
					throw new IllegalArgumentException("Error in '" + originalWords[c] + "': AM/PM already specified.");
				amStatus = 2;
				continue outer;
			}
			
			//check for weekday names
			for(int weekdayIndex = 0; weekdayIndex < WEEKDAY_PREFIXES.length; weekdayIndex++)
				if(word.startsWith(WEEKDAY_PREFIXES[weekdayIndex]))
				{
					if(out.significance.year)
						throw new IllegalArgumentException("Error in '" + originalWords[c] + "': year already specified.");
					if(out.significance.month)
						throw new IllegalArgumentException("Error in '" + originalWords[c] + "': month already specified.");
					if(out.significance.day)
						throw new IllegalArgumentException("Error in '" + originalWords[c] + "': day already specified.");
					out.calendar.set(DAY_OF_WEEK, WEEKDAYS[weekdayIndex]);
					out.significance.year = out.significance.month = out.significance.day = true;
					continue outer;
				}
			
			
			
			/*MATCH PATTERNS*/
			
			//match y-m-d and y/m/d
			String formatToken = null;
			if(word.split("-").length == 2)
				formatToken = "-";
			else if(word.split("/").length == 2)
				formatToken = "/";
			if(formatToken != null)
			{
				String[] entries = word.split(formatToken);
				try
				{
					int year = Integer.parseInt(entries[0]), month = Integer.parseInt(entries[1]),
							day = Integer.parseInt(entries[2]);
					if(out.significance.year)
						throw new IllegalArgumentException("Error in '" + originalWords[c] + "': year already specified");
					if(out.significance.month)
						throw new IllegalArgumentException("Error in '" + originalWords[c] + "': month already specified.");
					if(out.significance.day)
						throw new IllegalArgumentException("Error in '" + originalWords[c] + "': day already specified.");
					
					out.calendar.set(YEAR, year);
					out.calendar.set(MONTH, month);
					out.calendar.set(DAY_OF_MONTH, day);
					out.significance.year = out.significance.month = out.significance.day = true;
					
					continue outer;//successfully parsed according to the format
				}
				catch(NumberFormatException ex){}//then I guess the word doesn't match this format
			}
			
			//match yyyy and n (1-2 digit day)
			try
			{
				int value = Integer.parseInt(word);
				if(word.length() == 4)
				{
					if(out.significance.year)
						throw new IllegalArgumentException("Error in '" + originalWords[c] + "': year already specified");
					out.calendar.set(YEAR,  value);
					out.significance.year = true;
					continue outer;
				}
				if(word.length() == 1 || word.length() == 2)
				{
					if(out.significance.day)
						throw new IllegalArgumentException("Error in '" + originalWords[c] + "': day already specified");
					out.calendar.set(DAY_OF_MONTH, value);
					out.significance.day = true;
					continue outer;
				}
			}
			catch(NumberFormatException ex){}//then I guess the word doesn't match this format
			
			//match nth (day)
			if(word.endsWith("th"))
			{
				try
				{
					int value = Integer.parseInt(word.substring(0, word.length() - 2));
					if(out.significance.day)
						throw new IllegalArgumentException("Error in '" + originalWords[c] + "': day already specified");
					out.calendar.set(DAY_OF_MONTH, value);
					out.significance.day = true;
					continue outer;
				}
				catch(NumberFormatException ex){}//then I guess the word doesn't match this format
			}
			
			//match h:m
			if(word.contains(":") && word.charAt(word.length() - 1) != ':')
			{
				String[] halves = word.split(":");
				try
				{
					hour = Integer.parseInt(halves[0]);
					int minute = Integer.parseInt(halves[1]);
					if(out.significance.hour)
						throw new IllegalArgumentException("Error in '" + originalWords[c] + "': hour already specified.");
					if(out.significance.minute)
						throw new IllegalArgumentException("Error in '" + originalWords[c] + "': minute already specified.");
					out.calendar.set(MINUTE, minute);
					out.significance.hour = out.significance.minute = true;
					continue outer;
				}
				catch(NumberFormatException ex){}//then I guess the word doesn't match this format
			}
			
			//match h:
			if(word.charAt(word.length() - 1) == ':')
			{
				try
				{
					hour = Integer.parseInt(word.substring(0, word.length() - 1));
					if(out.significance.hour)
						throw new IllegalArgumentException("Error in '" + originalWords[c] + "': hour already specified.");
					continue outer;
				}
				catch(NumberFormatException ex){}//then I guess the word doesn't match this format
			}
			
			
			
			
			//if we reach this line, the word wasn't understood
			throw new IllegalArgumentException("Could not understand '" + originalWords[c] + "'");
		}
		
		
		
		
		
		if(at)
			throw new IllegalArgumentException("'at' wasn't followed by a time");
		
		if(out.significance.hour)
		{
			if(amStatus == 0)
				out.calendar.set(HOUR_OF_DAY, hour);
			else
			{
				out.calendar.set(HOUR, hour);
				out.calendar.set(AM_PM, amStatus == 1 ? AM : PM);
			}
		}
		
		
		if(!out.isReasonable())
			throw new IllegalArgumentException("This date/time isn't reasonable. Fill out fields in descending order.");//TODO better message
		
		
		return out;
	}
}
